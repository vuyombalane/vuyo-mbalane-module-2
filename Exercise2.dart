
void main() {

List<WinningApp> winningapps = [
  WinningApp(name: 'FNB Banking App', year: 2012),
  WinningApp(name: 'SnapScan', year: 2013),
  WinningApp(name: 'LIVE Inspect', year: 2014),
  WinningApp(name: 'WumDrop', year: 2015),
  WinningApp(name: 'Domestly', year: 2016),
  WinningApp(name: 'Shyft', year: 2017),
  WinningApp(name: 'Khula Ecosystem', year: 2018),
  WinningApp(name: 'Naked Insurance', year: 2019),
  WinningApp(name: 'EasyEquities', year: 2020),
  WinningApp(name: 'Ambani Africa', year: 2021)
];

winningapps.sort((a, b) => a.name.compareTo(b.name));
winningapps.forEach((winningapp) => print(winningapp.name));

print("Shyft 2017");
print("Khula Ecosystem 2018");

print(winningapps.length);

}


class WinningApp {
  String name; 
  int year;

  WinningApp({required this.name, required this.year});

}
